import java.util.Random;

/**
 * Created by bowenliu on 12/11/14.
 * this class generate a N dimension uniform random vector
 */

public class UniformRandomVectorGenerator implements RandomVectorGenerator {

        private int N;
        public UniformRandomVectorGenerator(int _N) {
            N = _N;
        }
        @Override
        public double[] getVector() {
            double[] vec = new double[N];
            Random rand = new Random();
            for (int i = 0; i < N; i++) {
                vec[i] = rand.nextDouble();
            }
            return vec;
        }

    }

