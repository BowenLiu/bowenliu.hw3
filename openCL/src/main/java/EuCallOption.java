
public class EuCallOption implements PayOut {

	private double K;

	/**
	 * constructor
	 * @param _K
	 * K is the strike
	 */
	
	public EuCallOption(double _K) {
		K = _K;
	}

	/**
	 *
	 * @param path
	 * @return pay out of a Eu call option
	 */

	@Override
	public double getPayout(PathGenerator path) {
		int m = path.getPath().size();
		double St = path.getPath().get(m - 1).getV();
		
		return Math.max(0., St - K);
	}

}
