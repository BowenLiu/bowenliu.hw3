


public class Stats {

	private double mean;
	private double variance;
	private double sumOfSquares;
	private int num;

	/**
	 * this method update basic stats in the class
	 * updated stats are mean, variance, sum of squares, and number of simulations
	 * @param price
	 */
	
	public void update(double price) {
		
		mean = (mean * num + price)/(num+1);
		sumOfSquares = (sumOfSquares * num + price * price)/(num+1);
		setVariance(sumOfSquares - mean * mean);
		num++;
	}


	/**
	 * @return the variance
	 */
	public double getVariance() {
		return variance;
	}

	/**
	 * @param variance the variance to set
	 */
	public void setVariance(double variance) {
		this.variance = variance;
	}
	
	public double getMean() {
		return mean;
	}
	

		
}
