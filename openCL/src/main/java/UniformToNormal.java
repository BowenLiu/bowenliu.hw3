import com.nativelibs4java.opencl.*;
import org.bridj.Pointer;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static org.bridj.Pointer.allocateFloats;



/**
 * this class is modified based on the example 6 given
 * only one static method to change
 */
public class UniformToNormal {

    public static float[] uniformToNormal(double[] uniform) {

        // Creating the platform which is out computer.
        CLPlatform clPlatform = JavaCL.listPlatforms()[0];
        // Getting the GPU device
        CLDevice device = clPlatform.getBestDevice();
        // Let's make a context
        CLContext context = JavaCL.createContext(null, device);
        // Lets make a default FIFO queue.
        CLQueue queue = context.createDefaultQueue();

        // Read the program sources and compile them :
        //this program change uniform random variable to normal random variable using Box Muller Transformation
        String src = "__kernel void uniform_to_normal(__global const float* a, __global float* out, int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i >= n)\n" +
                "        return;\n" +
                "\n" +
                "    out[2*i] = sqrt(-2*log(a[2*i]))*cos(2*3.14159265358*a[2*i+1]);\n" +
                "    out[2*i+1] = sqrt(-2*log(a[2*i]))*sin(2*3.14159265358*a[2*i+1]);\n" +
                "}";
        CLProgram program = context.createProgram(src);
        program.build();

        CLKernel kernel = program.createKernel("uniform_to_normal");

        final long tmp = System.currentTimeMillis();
        final int n = uniform.length;
        final Pointer<Float>
                aPtr = allocateFloats(n);

        for (int i = 0; i < n; i++) {
            aPtr.set(i, (float) uniform[i]);
        }

        // Create OpenCL input buffers (using the native memory pointers aPtr and bPtr) :
        CLBuffer<Float>
                a = context.createFloatBuffer(CLMem.Usage.Input, aPtr);

        // Create an OpenCL output buffer :
        CLBuffer<Float> out = context.createFloatBuffer(CLMem.Usage.Output, n);
        kernel.setArgs(a, out, n/2);
        CLEvent event = kernel.enqueueNDRange(queue, new int[]{n}, new int[]{128});
        final Pointer<Float> cPtr = out.read(queue,event);
        float[] normalvector = new float[n];
        normalvector = cPtr.getFloats();
        //release the space
        a.release();
        cPtr.release();
        aPtr.release();
        out.release();
        return normalvector;












    }

}
