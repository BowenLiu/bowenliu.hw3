import java.util.List;

import org.joda.time.DateTime;



public interface PathGenerator {
	public List<Pair<DateTime, Double>> getPath();

}
