import java.util.Random;


public class NormalRandomVectorGenerator implements RandomVectorGenerator {

	private int N;
	private UniformRandomVectorGenerator uniform;
	private float[] normal;
	int ind = 0;

	/**
	 * constructor
	 * generate 2 million uniform random vector
	 * save the transfered normal vector into the class
	 * @param _N
	 */
	public NormalRandomVectorGenerator(int _N) {
		N = _N;
		uniform = new UniformRandomVectorGenerator(2000000);
		normal = UniformToNormal.uniformToNormal(uniform.getVector());

	}

	/**
	 * use uniformtonormal to change uniform random vector to normal random vector
	 * in every simulation, we use N normal random numbers
	 * if 2million are used up we generate a new one
	 * @return N dimension normal random vector
	 */
	@Override
	public double[] getVector() {
		double[] vec = new double[N];
		if(N+ ind >= 2000000) {
			ind = 0;
			uniform = new UniformRandomVectorGenerator(2000000);
			normal = UniformToNormal.uniformToNormal(uniform.getVector());

		}
		for (int i = 0+ind; i < N+ind; i++) {
			vec[i-ind] = (double) normal[i];


		}
		ind = ind + N;

		return vec;
	}

}
