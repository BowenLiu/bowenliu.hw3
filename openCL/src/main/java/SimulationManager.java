import org.apache.commons.math3.distribution.NormalDistribution;
import org.joda.time.DateTime;


public class SimulationManager {


	public static void main(String[] args) {
		int N = 252;
		RandomVectorGenerator rvg = new AntiThetic(new NormalRandomVectorGenerator(N));
		DateTime d1 = new DateTime();
		int expiration = 252;
		DateTime d2 = d1.plusDays(expiration);
		double r = 0.0001;
		double vol = 0.01;
		double mkt = 152.35;
		double c = 0.96;
		double error = 0.01;
		double euStrike = 165;
		double asStrike = 164;
		final int CONST = 5000;
		
		PathGenerator pg;
		Stats st = new Stats();
		PayOut euPayOut = new EuCallOption(euStrike);

		/**
		 * get the estimation of variance of Eu call Option
		 * in order to use CLT to do Monte Carlo
		 */
		for (int j = 0; j < CONST; j++) {
			pg = new GBMPathGenerator(r, N, vol, mkt, d1, d2, rvg);
			st.update(euPayOut.getPayout(pg));
			
			
		}
		double varEst = st.getVariance();
		NormalDistribution nd = new NormalDistribution();
		double y = nd.inverseCumulativeProbability(1-(1-c)/2);
		int m = (int) ((y * Math.sqrt(varEst) / error) * (y * Math.sqrt(varEst) / error));

		/**
		 * get the price of a Eu call option
		 */

		PathGenerator path;
		Stats euCallStats = new Stats();
		for (int i = 0; i < m; i++) {
			path = new GBMPathGenerator(r, N, vol, mkt, d1, d2, rvg);
			euCallStats.update(euPayOut.getPayout(path));
		}

		double euPrice = Math.exp(-r * expiration) * euCallStats.getMean();
		System.out.println("EuropeanCallOptionPrice: " + euPrice);

		/**
		 * get the estimation of variance of AisanOption
		 * in order to use CLT to do Monte Carlo
		 */
		PathGenerator pg1;
		Stats st1 = new Stats();
		PayOut asPayOut = new AsiaCallOption(asStrike);
		
		for (int j = 0; j < CONST; j++) {
			pg1 = new GBMPathGenerator(r, N, vol, mkt, d1, d2, rvg);
			st1.update(asPayOut.getPayout(pg1));
			
		}
		double varEst1 = st1.getVariance();
		int m1 = (int) ((y * Math.sqrt(varEst1) / error) * (y * Math.sqrt(varEst1) / error));

		/**
		 * get the price of AsianOption
		 *
		 */
		PathGenerator path1;
		Stats asCallStats = new Stats();
		for (int i = 0; i < m1; i++) {
			path1 = new GBMPathGenerator(r, N, vol, mkt, d1, d2, rvg);
			asCallStats.update(asPayOut.getPayout(path1));
			
			
		}
		double asPrice = Math.exp(-r * expiration) * asCallStats.getMean();
		System.out.println("AsianCallOptionPrice: " + asPrice);
		
		
	}
}
