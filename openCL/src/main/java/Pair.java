
public class Pair<T1, T2> {
	/**
	 * u is the key, which is date
	 * v is the value, which is price
	 */

	private T1 u;
	private T2 v;
	public Pair(T1 _u, T2 _v){
		setU(_u);
		setV(_v);
	}
	/**
	 * @return the u
	 */
	public T1 getU() {
		return u;
	}
	/**
	 * @param u the u to set
	 */
	public void setU(T1 u) {
		this.u = u;
	}
	/**
	 * @return the v
	 */
	public T2 getV() {
		return v;
	}
	/**
	 * @param v the v to set
	 */
	public void setV(T2 v) {
		this.v = v;
	}
	
	
}
