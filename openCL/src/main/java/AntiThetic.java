import java.util.Arrays;


public class AntiThetic implements RandomVectorGenerator {

	/**
	 * use antithetic to help converge faster
	 * this class is given in the example
	 */


	private RandomVectorGenerator rvg;
	private double[] lastVector;

	public AntiThetic(RandomVectorGenerator _rvg) {
		rvg = _rvg;

	}
	@Override
	public double[] getVector() {
		if ( lastVector == null ){
			lastVector = rvg.getVector();
			return lastVector;
		} else {
			double[] tmp =Arrays.copyOf(lastVector, lastVector.length);
			lastVector = null;
			for (int i = 0; i < tmp.length; ++i){ tmp[i] = -tmp[i];}
			return tmp;
		}
	}

}
