import java.util.Iterator;

import org.joda.time.DateTime;


public class AsiaCallOption implements PayOut {


	private double K;

	/**
	 * constructor
	 * @param _K
	 * K is the strike
	 */
	
	public AsiaCallOption(double _K) {
		K = _K;
	}

	/**
	 *
	 * @param path
	 * @return pay out of an Asian option
	 */

	@Override
	public double getPayout(PathGenerator path) {
		double sum = 0;
		Iterator<Pair<DateTime, Double>> iter = path.getPath().iterator();
		while (iter.hasNext()) {
			Pair<DateTime, Double> tmp = iter.next();
			sum += tmp.getV();
		}
		int m = path.getPath().size();
		return Math.max(sum/m - K, 0);
	}

}
