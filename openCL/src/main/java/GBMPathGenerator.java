import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;


public class GBMPathGenerator implements PathGenerator {

	private double rate;
	private double sigma;
	private double S0;
	private int N;
	private DateTime startDate;
	private DateTime endDate;
	private RandomVectorGenerator rvg;

	/**
	 * constructor
	 * save all the parameters of a GBM path into the class
	 * @param rate
	 * @param N
	 * @param sigma
	 * @param S0
	 * @param startDate
	 * @param endDate
	 * @param rvg
	 */
	public GBMPathGenerator(double rate, int N,
			double sigma, double S0,
			DateTime startDate, DateTime endDate,
			RandomVectorGenerator rvg){
		this.startDate = startDate;
		this.endDate = endDate;
		this.rate = rate;
		this.S0 = S0;
		this.sigma = sigma;
		this.N = N;
		this.rvg = rvg;
	}

	/**
	 * use the parameters to construct a path
	 * @return a GBM path
	 */
	@Override
	public List<Pair<DateTime, Double>> getPath() {
		double[] n = rvg.getVector();
		DateTime current = new DateTime(startDate.getMillis());
		long delta = (endDate.getMillis() - startDate.getMillis())/N;
		List<Pair<DateTime, Double>> path = new ArrayList<Pair<DateTime,Double>>();//use arraylist to search ith element quicker than linkedlist
		path.add(new Pair<DateTime, Double>(current, S0));
		for ( int i=1; i < N; ++i){
			current.plusMillis((int) delta);
			path.add(new Pair<DateTime, Double>(current, 
					path.get(i-1).getV()*Math.exp((rate-sigma*sigma/2)+sigma * n[i-1])));
		}
		return path;
	}
}
