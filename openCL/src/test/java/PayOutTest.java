import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Test;


public class PayOutTest {

	/**
	 * test the outcome of payout function is right
	 */

	@Test
	public void test() {
		PayOut pt = new EuCallOption(10);
		DateTime d1 = new DateTime();
		int expiration = 252;
		DateTime d2 = d1.plusDays(expiration);
		RandomVectorGenerator rvg = new AntiThetic(new NormalRandomVectorGenerator(252));
		PathGenerator path = new GBMPathGenerator(0.01, 252, 0.1, 100, d1, d2, rvg);
		double pay = pt.getPayout(path);
		assertTrue(pay>=0);
	}

}
