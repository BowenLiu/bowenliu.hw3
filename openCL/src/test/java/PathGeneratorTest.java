import static org.junit.Assert.*;

import org.joda.time.DateTime;
import org.junit.Test;


public class PathGeneratorTest {

	/**
	 * this class test the path is generated with right length
	 */
	@Test
	public void test() {
		DateTime d1 = new DateTime();
		int expiration = 252;
		DateTime d2 = d1.plusDays(expiration);
		RandomVectorGenerator rvg = new AntiThetic(new NormalRandomVectorGenerator(252));
		PathGenerator path = new GBMPathGenerator(0.01, 252, 0.1, 100, d1, d2, rvg);
		assertTrue(path.getPath().size() == 252);
	}

}
