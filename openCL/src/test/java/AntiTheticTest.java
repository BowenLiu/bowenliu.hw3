import static org.junit.Assert.*;

import org.junit.Test;


public class AntiTheticTest {

	/**
	 * test the antithetic do generate an opposite sign random vector
	 */
	@Test
	public void test() {
		AntiThetic rvg = new AntiThetic(new NormalRandomVectorGenerator(3));
		double[] vec = rvg.getVector();
		double[] avec = rvg.getVector();
		assertEquals(vec[0]+avec[0],0, 0.01);
		assertEquals(vec[1]+avec[1],0, 0.01);
		assertEquals(vec[2]+avec[2],0, 0.01);
	}

}
